package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.samples.petclinic.owner.Pet;
import org.springframework.samples.petclinic.owner.PetRepository;
import org.springframework.samples.petclinic.owner.PetType;
import org.springframework.samples.petclinic.vet.Specialty;
import org.springframework.samples.petclinic.vet.Vet;
import org.springframework.samples.petclinic.vet.VetRepository;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VisitRepositoryTests {
	
	@Autowired
	private VisitRepository visits;
	@Autowired
	private VetRepository vets;
	
	private Visit visit;
	private Vet vet;
	
	@Before
	public void init() {
		if (this.visit==null) {
			
			if (this.vet==null) {
				vet = new Vet ();
				vet.setFirstName("Manuel");
				vet.setLastName("Nieto");
				vet.setHomeVisits(false);
				
				Collection <Specialty> vetSpec = this.vets.findSpecialties();
				
				if (!vetSpec.isEmpty()) {				
					for (Specialty spec : vetSpec) {
						vet.addSpecialty(spec);
					}				
				}			
				
				this.vets.save(vet);
			}
			
			LocalDate visityear = LocalDate.of(2018, Month.JANUARY, 26);
			visit = new Visit ();
			visit.setDate(visityear);
			visit.setDescription("visita planificada");
			visit.setPetId(1);
			visit.setVet(vet);
			
			this.visits.save(visit);
		}
	}
	
	
	
	@Test
	public void testFindById() {
		Visit visitFindById = this.visits.findById(visit.getId());
		
		assertNotNull(visitFindById.getDate());
		assertEquals(visitFindById.getDate(), visit.getDate());	
		
		assertNotNull(visitFindById.getDescription());
		assertEquals(visitFindById.getDescription(), visit.getDescription());	
		
		assertNotNull(visitFindById.getPetId());
		assertEquals(visitFindById.getPetId(), visit.getPetId());
		
		assertNotNull(visitFindById.getVet());
		assertEquals(visitFindById.getVet().getFirstName(), visit.getVet().getFirstName());
		assertEquals(visitFindById.getVet().getLastName(), visit.getVet().getLastName());
		assertFalse(visitFindById.getVet().getHomeVisits());
	}
	
	
	@Test
	public void testFindByIdNotEqual() {
		Visit visitSave = new Visit();
		LocalDate visityear = LocalDate.of(2017, Month.FEBRUARY, 14);
		visitSave.setDate(visityear);
		visitSave.setDescription("visita mal planificada");
		visitSave.setPetId(2);
		
		visitSave.setVet(vet);
		
		assertNull(visitSave.getId());
		
		this.visits.save(visitSave);
		
		assertNotNull(visitSave.getId());
		
		Visit visitFindById = this.visits.findById(this.visit.getId());
		
		assertNotNull(visitFindById.getDate());
		assertNotEquals(visitFindById.getDate(), visitSave.getDate());	
		
		assertNotNull(visitFindById.getDescription());
		assertNotEquals(visitFindById.getDescription(), visitSave.getDescription());	
		
		assertNotNull(visitFindById.getPetId());
		assertNotEquals(visitFindById.getPetId(), visitSave.getPetId());
	}
	
	
	
	@After
    //This test is executed after each test created in this suite
    public void finish() {
		this.vet=null;
    	this.visit=null;
    }
}