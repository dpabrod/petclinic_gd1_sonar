package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.Month;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class PetTests {
	public static Pet pet;

	@BeforeClass
	// This method is called before executing this suite
	public static void initClass() {
		LocalDate a = LocalDate.of(2000, Month.MAY, 20);
		PetType p = new PetType();
		p.setName("Dog");
		pet = new Pet();
		pet.setName("Moro");
		pet.setBirthDate(a);
		pet.setType(p);
	}

	@Before
	// This test is executed before each test created in this suite
	public void init() {
		pet.setWeight(8);
		pet.setComments("Nervioso");

	}

	@Test
	public void testPet() {
		assertNotNull(pet);
	}

	@Test
	public void testGetWeight() {
		assertNotNull(pet.getWeight());
		assertTrue(pet.getWeight() == 8);
	}

	@Test
	public void testSetWeight() {
		assertNotNull(pet.getWeight());
		assertTrue(pet.getWeight() == 8);
		pet.setWeight(10);
		assertFalse(pet.getWeight() != 10);
		assertTrue(pet.getWeight() == 10);
	}

	@Test
	public void testGetComments() {
		assertNotNull(pet.getComments());
		assertTrue(pet.getComments() == "Nervioso");
	}

	@Test
	public void testSetComments() {
		assertNotNull(pet.getComments());
		assertTrue(pet.getComments() == "Nervioso");
		pet.setComments("Agresivo");
		assertFalse(pet.getComments() != "Agresivo");
		assertTrue(pet.getComments() == "Agresivo");
	}

	@Ignore
	// This test is not executed
	public void ignore() {
		pet.setWeight(6);
		pet.setComments("Tranquilo");
	}

	@After
	// This test is executed after each test created in this suite
	public void finish() {
		pet.setWeight(5);
		pet.setComments("Jugueton");
	}

	@AfterClass
	// This method is executed after all the tests included in this suite are
	// completed.
	public static void finishClass() {
		pet = null;
	}

}