package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTests {

	@Autowired
	private PetRepository pets;

	private Pet pet;

	@Autowired
	private OwnerRepository owners;

	private Owner o;

	@Before
	public void init() {
		if (this.o == null) {
			o = new Owner();
			o.setFirstName("Antonio");
			o.setLastName("Redondo");
			o.setAddress("8 Moctezuma");
			o.setCity("Caceres");
			o.setTelephone("695326874");
		}
		this.owners.save(o);
		if (this.pet == null) {
			LocalDate a = LocalDate.of(2000, Month.MAY, 20);
			PetType p = new PetType();
			p.setId(1);
			p.setName("cat");
			pet = new Pet();
			pet.setName("Pepito");
			pet.setBirthDate(a);
			pet.setComments("Nervioso");
			pet.setWeight(8);
			pet.setOwner(o);
			pet.setType(p);

		}
		this.pets.save(pet);
	}

	@Test
	public void testDeletePet() {
		Pet p2 = this.pets.findById(pet.getId());
		assertNotNull(p2.getName());
		assertEquals(p2.getName(), pet.getName());

		assertNotNull(p2.getBirthDate());
		assertEquals(p2.getBirthDate(), pet.getBirthDate());

		assertNotNull(p2.getType());
		assertEquals(p2.getType(), pet.getType());

		assertNotNull(p2.getWeight());
		assertTrue(p2.getWeight() == pet.getWeight());

		assertNotNull(p2.getComments());
		assertEquals(p2.getComments(), pet.getComments());
		this.pets.deletePetInfo(pet.getId());
		this.pets.delete(pet);
		assertNull(this.pets.findById(pet.getId()));

	}

	@Test
	public void testFindPetsByName() {
		Collection<Pet> pets2 = pets.findPetsByName("Pepito");
		assertFalse(pets2.isEmpty());
		pets2 = pets.findPetsByName("No existe");
		assertTrue(pets2.isEmpty());
	}

	@After
	public void finish() {
		this.pet = null;
	}
}
