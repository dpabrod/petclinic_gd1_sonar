package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class OwnerRepositoryTests {

	@Autowired
	private OwnerRepository owners;

	private Owner o;

	@Before
	public void init() {
		if (this.o == null) {
			o = new Owner();
			o.setFirstName("Antonio");
			o.setLastName("Redondo");
			o.setAddress("8 Moctezuma");
			o.setCity("Caceres");
			o.setTelephone("695326874");
		}
		this.owners.save(o);
	}

	@Test
	 public void testDeleteById() {
		 //Comprobamos que se haya insertado bien
		 Owner o2= this.owners.findById(o.getId());
		 assertNotNull(o2.getFirstName());
		 assertEquals(o2.getFirstName(), o.getFirstName());
		 
		 assertNotNull(o2.getLastName());
		 assertEquals(o2.getLastName(), o.getLastName());
		 
		 assertNotNull(o2.getAddress());
		 assertEquals(o2.getAddress(), o.getAddress());
		 
		 assertNotNull(o2.getCity());
		 assertEquals(o2.getCity(), o.getCity());
		 
		 assertNotNull(o2.getTelephone());
		 assertEquals(o2.getTelephone(), o.getTelephone());
		
		 this.owners.deleteById(o.getId());
		 //Comprobamos que se haya borrado
		 assertNull(this.owners.findById(o.getId()));
	 }
	@After
	public void finish() {
		this.o=null;
	}
}
